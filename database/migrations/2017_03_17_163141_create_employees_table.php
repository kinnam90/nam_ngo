<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id', true);
            $table->string('lastname' );
            $table->string('firstname');
            // $table->string('middlename');
            $table->string('address');
            $table->string('picture');

            // $table->tinyInteger('level')->default(0);
            $table->tinyInteger('first_log')->default(0);

            // $table->integer('city_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('age')->unsigned();
            $table->integer('division_id')->unsigned();
            $table->integer('department_id')->unsigned();
            // id_chuc_vu 

            
            $table->foreign('country_id')->references('id')->on('country');
            $table->foreign('department_id')->references('id')->on('department');
            $table->foreign('division_id')->references('id')->on('division');

            // $table->char('zip');
            
            $table->date('birthdate');
            $table->date('date_hired');
                                         
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
